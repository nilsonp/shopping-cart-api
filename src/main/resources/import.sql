
INSERT INTO customer (email, name, password) VALUES ('customer1@mail.com', 'Customer 1', 'Abc123');

INSERT INTO customer (email, name, password) VALUES ('two.customer@mail.com', 'Customer Dos', 'Abc123');

INSERT INTO product (code, name, description, price, qty_stock) VALUES('P001', 'Producto 1', 'abc zxc', 10.5, 2);

INSERT INTO product (code, name, description, price, qty_stock) VALUES('P003', 'Producto 3', 'qwerty', 7, 6);

INSERT INTO product (code, name, description, price, qty_stock) VALUES('P004', 'Producto 4', 'zxczc', 5.6, 0);

INSERT INTO shopping_car (id, creation_date, customer_email) VALUES (1, '2022-07-27', 'customer1@mail.com');
INSERT INTO line_item (shopping_car_id, product_code, quantity, amount) VALUES (1, 'P004', 2, 11.2);
INSERT INTO line_item (shopping_car_id, product_code, quantity, amount) VALUES (1, 'P001', 1, 10.5);

INSERT INTO shopping_car (id, creation_date, customer_email) VALUES (2, '2022-07-28', 'two.customer@mail.com');
INSERT INTO line_item (shopping_car_id, product_code, quantity, amount) VALUES (2, 'P003', 1, 7);