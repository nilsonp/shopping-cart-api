package co.com.cce.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
@IdClass(LineItemID.class)
public class LineItem implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@ManyToOne(cascade = CascadeType.ALL)
    private ShoppingCar shoppingCar;
    
	@Id
	@OneToOne(cascade = CascadeType.ALL)
    private Product product;
	
	@Column
    private int quantity;
    
	@Column
    private float amount;
    
	public LineItem() {
		super();
	}

	public ShoppingCar getShoppingCar() {
		return shoppingCar;
	}

	public void setShoppingCar(ShoppingCar shoppingCar) {
		this.shoppingCar = shoppingCar;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float price) {
		this.amount = price;
	}
    
}
