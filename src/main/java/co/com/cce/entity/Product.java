package co.com.cce.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Product implements Serializable{

	private static final long serialVersionUID = 1L;
    
	@Id
    private String code;
    
	@Column
    private String name;

	@Column
    private String description;

	@Column
    private int qtyStock;
	
	@Column
	private float price;

	@Transient
    private Set<LineItem> lineItems;
	
	public Product() {
		super();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getQtyStock() {
		return qtyStock;
	}

	public void setQtyStock(int qtyStock) {
		this.qtyStock = qtyStock;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Set<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(Set<LineItem> lineItems) {
		this.lineItems = lineItems;
	}

}
