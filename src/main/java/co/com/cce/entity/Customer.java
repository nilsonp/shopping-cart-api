package co.com.cce.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;


@Entity
public class Customer implements Serializable{

	private static final long serialVersionUID = 1L;
    
	@Column
    private String name;
    
	@Id
    private String email;
    
	@Column
    private String password;
    
	@Transient
    private ShoppingCar shoppingCart;

	public Customer() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ShoppingCar getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCar shoppingCart) {
		this.shoppingCart = shoppingCart;
	}
    
}
