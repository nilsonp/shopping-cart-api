package co.com.cce.entity;

import java.io.Serializable;

public class LineItemID implements Serializable{

	private static final long serialVersionUID = 1L;
	
    private ShoppingCar shoppingCar;
    
    private Product product;

	public ShoppingCar getShoppingCar() {
		return shoppingCar;
	}

	public void setShoppingCar(ShoppingCar shoppingCar) {
		this.shoppingCar = shoppingCar;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
