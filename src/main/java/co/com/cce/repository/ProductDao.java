package co.com.cce.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.cce.entity.Product;

public interface ProductDao extends CrudRepository<Product, String> {
    
    public Product findByCode(String code);
    
}
