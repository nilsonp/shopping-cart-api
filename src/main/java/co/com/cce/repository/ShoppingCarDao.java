package co.com.cce.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.cce.entity.ShoppingCar;

public interface ShoppingCarDao extends CrudRepository<ShoppingCar, Long> {
    
}
