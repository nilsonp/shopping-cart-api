package co.com.cce.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.cce.entity.LineItem;
import co.com.cce.entity.LineItemID;

public interface LineItemDao extends CrudRepository<LineItem, LineItemID> {
    
    public LineItem findByProduct(String code);
    
    public LineItem findByProductAndShoppingCar(String product, Long shoppingCar);
    
}
