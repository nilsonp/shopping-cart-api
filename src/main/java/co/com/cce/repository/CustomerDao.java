package co.com.cce.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.cce.entity.Customer;

public interface CustomerDao extends CrudRepository<Customer, Long> {
    
    public Customer findByEmail(String email);
    
    public Customer findByName(String name);
    
}
