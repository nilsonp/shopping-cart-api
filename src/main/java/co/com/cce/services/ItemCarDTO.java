package co.com.cce.services;

public class ItemCarDTO {
	
	private long idCar;
	
	private String codeProduct;
	
	private int quantity;

	public ItemCarDTO() {
		super();
	}

	public long getIdCar() {
		return idCar;
	}

	public void setIdCar(long idCar) {
		this.idCar = idCar;
	}

	public String getCodeProduct() {
		return codeProduct;
	}

	public void setCodeProduct(String codeProduct) {
		this.codeProduct = codeProduct;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
