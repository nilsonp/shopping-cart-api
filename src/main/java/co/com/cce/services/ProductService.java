package co.com.cce.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.cce.entity.Product;
import co.com.cce.repository.ProductDao;

@Service
public class ProductService implements IProductService {
	
	@Autowired
	private ProductDao productDao;

	@Override
	public List<ProductDTO> listProducts() {
		List<ProductDTO> listProducts = new ArrayList<>();
		productDao.findAll().forEach(p -> {
			ProductDTO pDto = new ProductDTO();
			pDto.setCode( p.getCode() );
			pDto.setDescription( p.getDescription() );
			pDto.setName( p.getName() );
			pDto.setQuantity( p.getQtyStock() );
			pDto.setAmount(p.getPrice());
			listProducts.add(pDto);
		});
		
		return listProducts;
	}

	@Override
	public boolean checkStock(String codeProduct) {
		Product product = productDao.findByCode(codeProduct);
		return product.getQtyStock() > 0 ? true : false;
	}
    
    
}
