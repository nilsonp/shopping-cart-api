package co.com.cce.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.cce.entity.LineItem;
import co.com.cce.entity.Product;
import co.com.cce.entity.ShoppingCar;
import co.com.cce.repository.LineItemDao;
import co.com.cce.repository.ProductDao;
import co.com.cce.repository.ShoppingCarDao;

@Service
public class CarService implements ICarService {
	
	@Autowired
	private ShoppingCarDao shoppingCartDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private LineItemDao lineItemDao;

	@Override
	public ItemCarDTO addProduct(ItemCarDTO addCarDTO) {
		
		Product product = productDao.findByCode(addCarDTO.getCodeProduct());
		Optional<ShoppingCar> shopingCart = shoppingCartDao.findById(addCarDTO.getIdCar());
		float amountLine = product.getPrice() * addCarDTO.getQuantity();
		
		LineItem lineItem = new LineItem();
		lineItem.setAmount(amountLine);
		lineItem.setProduct(product);
		lineItem.setQuantity(addCarDTO.getQuantity());
		lineItem.setShoppingCar(shopingCart.get());
		
		lineItemDao.save(lineItem);
		
		return addCarDTO;
		
	}

	@Override
	public ItemCarDTO updateProduct(ItemCarDTO updateCarDTO) {
		
		LineItem lineItem = lineItemDao.findByProductAndShoppingCar(
				updateCarDTO.getCodeProduct(), 
				updateCarDTO.getIdCar());
		
		lineItem.setQuantity(updateCarDTO.getQuantity());
		
		lineItemDao.save(lineItem);
		return updateCarDTO;		
	}

	@Override
	public ItemCarDTO removeProduct(ItemCarDTO removeCarDTO) {
		LineItem lineItem = lineItemDao.findByProductAndShoppingCar(
				removeCarDTO.getCodeProduct(), 
				removeCarDTO.getIdCar());
		
		lineItemDao.delete(lineItem);
		return removeCarDTO;
	}

	@Override
	public List<CarDTO> listCar() {
		List<CarDTO> listCars = new ArrayList<>();
		
		shoppingCartDao.findAll().forEach(c ->{
			CarDTO car = new CarDTO();
			List<ProductDTO> listProducts = new ArrayList<>();
			
			car.setIdCart(c.getId());
			car.setEmailCustomer(c.getCustomer().getEmail());
			car.setDate(c.getCreationDate());
			c.getItems().forEach(item -> {
				ProductDTO p = new ProductDTO();
				p.setCode(item.getProduct().getCode());
				p.setDescription(item.getProduct().getDescription());
				p.setName(item.getProduct().getName());
				p.setAmount(item.getAmount());
				p.setQuantity(item.getQuantity());
				listProducts.add(p);
			});
			car.setProducts(listProducts);
			listCars.add(car);
		});
		return listCars;
	}
        
}
