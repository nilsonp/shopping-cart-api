package co.com.cce.services;

import java.util.List;

public interface IProductService {

	public List<ProductDTO> listProducts();
	
	public boolean checkStock(String codeProduct);
}
