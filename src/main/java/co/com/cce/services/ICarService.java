package co.com.cce.services;

import java.util.List;

public interface ICarService {
	
	public ItemCarDTO addProduct(ItemCarDTO addCarDTO);
	
	public ItemCarDTO updateProduct(ItemCarDTO updateCarDTO);
	
	public ItemCarDTO removeProduct(ItemCarDTO removeCarDTO);
	
	public List<CarDTO> listCar();

}
