package co.com.cce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.cce.services.CarDTO;
import co.com.cce.services.ICarService;
import co.com.cce.services.ItemCarDTO;

@RestController
@RequestMapping(path = "/shopping-car")
public class CarController {

	@Autowired
    private ICarService cartService;
    
	@GetMapping
	public ResponseEntity<?> listCarts() {
		List<CarDTO> listCars = cartService.listCar();
		ResponseEntity<?> response = new ResponseEntity<>(listCars, HttpStatus.OK);
        return response;
	}
	
	@PostMapping
    public ResponseEntity<?> addProduct(RequestEntity<ItemCarDTO> itemDTO) {
		
		ItemCarDTO addItem = cartService.addProduct(itemDTO.getBody());
		ResponseEntity<?> response = new ResponseEntity<>(addItem, HttpStatus.OK);
        return response;
    }

	@PutMapping
    public ResponseEntity<?> updateProduct(RequestEntity<ItemCarDTO> itemDTO) {
		ItemCarDTO updateItem = cartService.updateProduct(itemDTO.getBody());
		ResponseEntity<?> response = new ResponseEntity<>(updateItem, HttpStatus.OK);
        return response;
    }

	@DeleteMapping
    public ResponseEntity<?> removeProduct(RequestEntity<ItemCarDTO> itemDTO) {
		ItemCarDTO deleteItem = cartService.removeProduct(itemDTO.getBody());
		ResponseEntity<?> response = new ResponseEntity<>(deleteItem, HttpStatus.OK);
        return response;
    }
    
}
