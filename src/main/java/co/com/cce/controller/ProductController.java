package co.com.cce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.cce.services.IProductService;
import co.com.cce.services.ProductDTO;

@RestController
@RequestMapping(path = "/product")
public class ProductController {
    
	@Autowired
    private IProductService productService;
    
	@GetMapping(path = "/list")
    public ResponseEntity<List<ProductDTO>> listProducts() {
		
		List<ProductDTO> listProducts = productService.listProducts();
		ResponseEntity<List<ProductDTO>> response = new ResponseEntity<>(listProducts, HttpStatus.OK);
		return response;
    }
    
    
}
