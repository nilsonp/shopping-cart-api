### Shopping car project 


_Diagrama de clases_ [Class model](https://gitlab.com/nilsonp/shopping-cart-api/-/wikis/Class-model)

_Coleccion postman_ [postman](https://gitlab.com/nilsonp/shopping-cart-api/-/wikis/Postman-Collection)


### Ejecutar el proyecto
Se puede ejecutar el proyecto de dos maneras:

* Docker:
	_Prerequisitos:_ docker y docker-compose instalado
	en el directorio raiz ejecutar el comando:
		`docker-compose up --build`
	  
* Maven:
	_Prerequisitos:_ JDK 11 o superior instalado
	en el directorio raiz ejecutar el comando:
		`mvnw spring-boot:run`

El servicio quedara disponible en la URL:
	`http://localhost:8080`

Utilizar la coleccion postman para realizar peticiones a los endpoints. [link](https://gitlab.com/nilsonp/shopping-cart-api/-/wikis/Postman-Collection)

### Endpoints disponibles

| **Metodo** | **url** | **Descripcion** | 
|--|--|--|
| GET | 'http://localhost:8080/product/list' | Lista los productos |
| GET | 'http://localhost:8080/shopping-car' | Lista los carros de compra disponibles |
| POST | 'http://localhost:8080/shopping-car' | Agregar un producto al carro de compras |
| PUT | 'http://localhost:8080/shopping-car' | Actualizar un producto del carro de compras|
| DELETE | 'http://localhost:8080/shopping-car' | Remover producto del carro de compras |
